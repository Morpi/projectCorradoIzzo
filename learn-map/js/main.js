$(document).ready(function() {
	function heightCalc() {
		$('.learn__section').hasClass('complete')&&$('.learn__marker').addClass('completed');

		const sectionCompleted = $('.learn__section.complete');
		const heightArr = [];

		for(let section of sectionCompleted) {
			if($(section).prev('.learn__title').length) {
				heightArr.push($(section)
					.prev('.learn__title')
					.outerHeight(true));
			}

			heightArr.push($(section).outerHeight(true));
		}
		const lastArrEl = heightArr[heightArr.length-1];
		const lastArrElNewHeight = $(sectionCompleted[sectionCompleted.length-1])
			.children('.learn__section-title')
			.outerHeight(true)/2;
		
		const sumOfHeight = heightArr
			.reduce((cur, acum) => cur+acum)-lastArrEl+lastArrElNewHeight;
		
		$('#marker-completed').css('height', sumOfHeight+'px');
	}
	heightCalc()

	//---collapse effect---
	$('.learn__toggler').each(function() {
		$(this).on('click', function() {
			$(this).parent().toggleClass('open');

			heightCalc()
		})
	})
})