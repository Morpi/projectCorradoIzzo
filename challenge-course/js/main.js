$(document).ready(function() {
	$('.challenge__menu-item').each(function() {
		$(this).on('click', function() {
			$('.challenge__menu-item').removeClass('active');
			$(this).addClass('active');

			$('.challenge__todo').removeClass('show');
			$('#' + $(this).data('target')).addClass('show');
		})
	})
})